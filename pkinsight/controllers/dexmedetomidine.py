from bokeh.models import ColumnDataSource
from bokeh.plotting import figure
from bokeh.layouts import column

from pkinsight.kafka import syringe_consumer
from pkinsight.models import Dexmedetomidine


class DexmedetomidineController:
    def __init__(self, config) -> None:
        self.config = config
        self.pksource = ColumnDataSource(data=dict(x=[], Cp=[], E=[]))
        self.dwc_source = ColumnDataSource(data={"x": [], "y": []})
        self.sim_source = ColumnDataSource(data={"x": [], "Cp": [], "E": []})
        self.model = Dexmedetomidine(config["weight"])

    def update(self):
        dt, cc, e = self.model.run_pkpd()
        new_data = {"x": [dt], "Cp": [cc], "E": [e]}
        self.pksource.stream(new_data, rollover=self.config["rollover"])

    def build_widgets(self):
        plot_cc = figure(title="Plasma", width=1000, height=300, x_axis_type="datetime")
        plot_cc.line(x="x", y="Cp", source=self.pksource)
        # plot_cc.legend.location = "top_left"

        plot_e = figure(
            title="Effet BIS", width=1000, height=300, x_axis_type="datetime"
        )
        # plot_e.line(
        #     x="x", y="y", source=self.dwc_source, legend_label="Mesuré", color="red"
        # )
        plot_e.line(
            x="x", y="E", source=self.pksource, legend_label="Estimé", color="blue"
        )
        plot_e.line(
            x="x",
            y="E",
            source=self.sim_source,
            legend_label="Simulé",
            color="magenta",
        )
        plot_e.legend.location = "top_left"
        return column(plot_cc, plot_e)

    async def run(self):
        syringe_reader = syringe_consumer(
            self.config["bloc"],
            drug=self.config["drug"],
            syringeid=self.config["syringeid"],
        )

        async for data in syringe_reader:
            match data:
                case ["syringe", data]:
                    # # TODO Temporary hack: Convert from ml/h to ug/s for conc = 5 ug/ml
                    self.model.rate = data["rate"] / 720

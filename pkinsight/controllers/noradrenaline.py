from aiostream import stream
from datetime import datetime, timedelta
from bokeh.layouts import column, row
from bokeh.models import ColumnDataSource
from bokeh.plotting import figure
from bokeh.models.widgets.inputs import NumericInput
from bokeh.models.widgets.buttons import Button

from pkinsight.kafka import dwc_consumer, syringe_consumer
from pkinsight.models import Noradrenaline
from pkinsight.utils import calculate_drug_dose


class NoradrenalineController:
    def __init__(self, config) -> None:
        self.config = config
        self.pksource = ColumnDataSource(data={"x": [], "Cp": [], "E": []})
        self.map_source = ColumnDataSource(data={"x": [], "y": []})
        self.sim_source = ColumnDataSource(data={"x": [], "Cp": [], "E": []})
        self.model = Noradrenaline()
        self.effect_truth = None
        self.sim_input_bolus = None
        self.sim_input_infusion = None
        self.sim_data = None

    def build_widgets(self):
        plot_cc = figure(
            title="Plasma (nmol/l)", width=1000, height=300, x_axis_type="datetime"
        )
        plot_cc.line(
            x="x", y="Cp", source=self.pksource, legend_label="Estimée", color="blue"
        )
        plot_cc.line(
            x="x",
            y="Cp",
            source=self.sim_source,
            legend_label="Simulée",
            color="magenta",
        )
        plot_cc.legend.location = "top_left"

        plot_e = figure(
            title="Effet PAM (mmHg)", width=1000, height=300, x_axis_type="datetime"
        )
        plot_e.line(
            x="x", y="y", source=self.map_source, legend_label="Mesurée", color="red"
        )
        plot_e.line(
            x="x", y="E", source=self.pksource, legend_label="Estimée", color="blue"
        )
        plot_e.line(
            x="x",
            y="E",
            source=self.sim_source,
            legend_label="Simulée",
            color="magenta",
        )
        plot_e.legend.location = "top_left"

        col_plots = column(plot_cc, plot_e)
        input_bolus = NumericInput(title="Bolus dose (ug)")
        input_bolus.on_change("value", self.sim_input_bolus_handler)
        input_infusion = NumericInput(title="Infusion rate (ug/h)")
        input_infusion.on_change("value", self.sim_input_infusion_handler)
        button_simulate = Button(label="Run Simulation")
        button_simulate.on_event("button_click", self.simulate)

        col_sim = column(input_bolus, input_infusion, button_simulate)

        return row(col_plots, col_sim)

    def sim_input_bolus_handler(self, attr, old, new):
        # TODO tmp hack convert from ug to nmol
        if new is not None:
            new = new * 5.91
        self.sim_input_bolus = new

    def sim_input_infusion_handler(self, attr, old, new):
        # TODO tmp hack convert from ug/ to nmol/s
        if new is not None:
            new = new * 5.91 / 3600
        self.sim_input_infusion = new

    def update(self):
        dt, cc, e = self.model.run_pkpd()
        new_data = {"x": [dt], "Cp": [cc], "E": [e]}
        self.pksource.stream(new_data, rollover=self.config["rollover"])
        if self.effect_truth:
            new_data = {"x": [dt], "y": [self.effect_truth]}
            self.map_source.stream(new_data, rollover=self.config["rollover"])
        if self.sim_data:
            # self.sim_source.stream(self.sim_data, rollover=self.config["rollover"])
            self.sim_source.data = self.sim_data
            self.sim_data = None

    def simulate(self, new):
        dt_upto = datetime.now() + timedelta(minutes=10)
        dt, Cc, E = self.model.simulate_pk(
            dt_upto, self.sim_input_bolus, self.sim_input_infusion
        )
        # new_data = {"x": [dt], "Cp": [Cc], "E": [E]}
        self.sim_data = {"x": dt, "Cp": Cc, "E": E}
        # print(new_data)
        # self.sim_source.stream(new_data, rollover=self.config["rollover"])

    async def run(self):
        syringe_reader = syringe_consumer(
            self.config["bloc"],
            drug=self.config["drug"],
            syringeid=self.config["syringeid"],
        )
        dwc_reader = dwc_consumer(self.config["bloc"], ["PNIm", "PAm", "ARTm"])
        combined_reader = stream.merge(syringe_reader, dwc_reader)

        async with combined_reader.stream() as streamer:
            async for data in streamer:
                match data:
                    case ["syringe", data]:
                        # d = calculate_drug_dose(data)
                        # # TODO Temporary hack: Convert from ml/h to nmol/s for conc = 5 ug/ml
                        self.model.rate = data["rate"] / 229.8672
                    case ["dwc", data]:
                        self.effect_truth = data["value"]

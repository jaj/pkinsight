import json
import ssl
import time
from datetime import datetime
from os import getenv

from aiokafka import AIOKafkaConsumer
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroDeserializer
from confluent_kafka.serialization import MessageField, SerializationContext


conf = {
    "username": getenv("PKINSIGHT_USERNAME"),
    "password": getenv("PKINSIGHT_PASSWORD"),
    "host": getenv("PKINSIGHT_BOOTSTRAP"),
    "protocol": getenv("PKINSIGHT_PROTOCOL"),
    "mechanism": getenv("PKINSIGHT_MECHANISM"),
    "syringe_topic": getenv("PKINSIGHT_SYRINGE_TOPIC"),
    "dwc_topic": getenv("PKINSIGHT_DWC_TOPIC"),
    "registry_url": getenv("PKINSIGHT_REGISTRY_URL", "http://10.130.192.132:8181"),
}


async def dwc_consumer(bedlabel, labels, get_history=False):
    ssl_context = ssl.create_default_context()
    ssl_context.check_hostname = False
    ssl_context.verify_mode = ssl.CERT_NONE

    consumer = AIOKafkaConsumer(
        bootstrap_servers=conf["host"],
        ssl_context=ssl_context,
        security_protocol=conf["protocol"],
        sasl_mechanism=conf["mechanism"],
        sasl_plain_password=conf["password"],
        sasl_plain_username=conf["username"],
    )
    consumer.subscribe([conf["dwc_topic"]])

    registry_client = SchemaRegistryClient({"url": conf["registry_url"]})
    schema = registry_client.get_latest_version(f'{conf["dwc_topic"]}-value').schema
    deserializer = AvroDeserializer(registry_client, schema)
    serialization_context = SerializationContext(conf["dwc_topic"], MessageField.VALUE)

    await consumer.start()

    if get_history:
        ten_minutes_ago = int((time.time() - 10 * 60) * 1000)
        partitions = consumer.assignment()
        partition_to_timestamp = {part: ten_minutes_ago for part in partitions}
        # end_offsets = consumer.end_offsets(list(partition_to_timestamp.keys()))
        mapping = await consumer.offsets_for_times(partition_to_timestamp)
        for partition, ts in mapping.items():
            if ts is None:
                continue
            # end_offset = end_offsets.get(partition)
            consumer.seek(partition, ts[0])

    try:
        async for bmsg in consumer:
            msg = deserializer(bmsg.value, serialization_context)
            try:
                if msg["BEDLABEL"] != bedlabel:
                    continue
                sublabel = msg["SUBLABEL"]
                if sublabel not in labels:
                    continue
                value = msg["VALUE"]
            except KeyError:
                continue
            dt = datetime.fromisoformat(msg["TIMESTAMP"])
            yield ("dwc", {"datetime": dt, "sublabel": sublabel, "value": value})
    finally:
        await consumer.stop()


async def syringe_consumer(bedlabel, drug=None, syringeid=None, get_history=False):
    ssl_context = ssl.create_default_context()
    ssl_context.check_hostname = False
    ssl_context.verify_mode = ssl.CERT_NONE

    consumer = AIOKafkaConsumer(
        bootstrap_servers=conf["host"],
        ssl_context=ssl_context,
        security_protocol=conf["protocol"],
        sasl_mechanism=conf["mechanism"],
        sasl_plain_password=conf["password"],
        sasl_plain_username=conf["username"],
    )
    consumer.subscribe([conf["syringe_topic"]])
    await consumer.start()

    if get_history:
        ten_minutes_ago = int((time.time() - 10 * 60) * 1000)
        partitions = consumer.assignment()
        partition_to_timestamp = {part: ten_minutes_ago for part in partitions}
        # end_offsets = consumer.end_offsets(list(partition_to_timestamp.keys()))
        mapping = await consumer.offsets_for_times(partition_to_timestamp)
        for partition, ts in mapping.items():
            if ts is None:
                continue
            # end_offset = end_offsets.get(partition)
            consumer.seek(partition, ts[0])

    try:
        # topic, partition, offset, key, value, timestamp
        async for bmsg in consumer:
            if bmsg.key != f'"{bedlabel}"'.encode("utf-8"):
                continue
            msg = json.loads(bmsg.value.decode("utf-8"))
            if "syringes" not in msg:
                print(msg)
            for msg_syringeid, syringedata in msg["syringes"].items():
                if drug and syringedata.get("drug", "") != drug:
                    continue
                elif syringeid and msg_syringeid != syringeid:
                    continue

                try:
                    rate = float(syringedata["rate"])
                except (KeyError, ValueError):
                    continue

                try:
                    rate_unit = syringedata["rate_unit"]
                except KeyError:
                    rate_unit = "ml/h"

                try:
                    volume = syringedata["volume"]
                except KeyError:
                    continue

                try:
                    volume_unit = syringedata["volume_unit"]
                except KeyError:
                    volume_unit = "ml"

                try:
                    drug_concentration = syringedata["drug_concentration"]
                except KeyError:
                    drug_concentration = None

                yield (
                    "syringe",
                    {
                        "rate": rate,
                        "rate_unit": rate_unit,
                        "volume": volume,
                        "volume_unit": volume_unit,
                        "drug_concentration": drug_concentration,
                    },
                )
    finally:
        await consumer.stop()

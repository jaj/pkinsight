from bokeh.plotting import curdoc
from tornado.ioloop import IOLoop

from pkinsight.controllers import DexmedetomidineController, NoradrenalineController
from pkinsight.utils import get_config

fs = 2
n_minutes = 30
rollover = int(n_minutes * 60 * fs)

doc = curdoc()
args = doc.session_context.request.arguments
print(args)

conf = get_config(args)
conf["rollover"] = rollover

modelname = conf["model"]
if modelname.startswith("baby"):
    controller = NoradrenalineController(conf)
elif modelname.startswith("dex"):
    controller = DexmedetomidineController(conf)
else:
    raise ValueError(modelname)

root_model = controller.build_widgets()
doc.add_root(root_model)

IOLoop.current().add_callback(controller.run)
doc.add_periodic_callback(controller.update, int(1000 / fs))

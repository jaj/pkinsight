from datetime import datetime
from typing import List, Optional

import numpy as np
from scipy.integrate import solve_ivp


class Model:
    def __init__(
        self, t0: Optional[datetime] = None, y0: List[float] = [0, 0, 0]
    ) -> None:
        self._dt = t0 if t0 is not None else datetime.now()
        self._y = y0
        self._rate = 0

    @property
    def rate(self):
        return self._rate

    @rate.setter
    def rate(self, value):
        self.run_pk()
        self._rate = float(value)

    def pk(self, t, y, Inf, p):
        raise NotImplementedError

    def calc_cc(self):
        raise NotImplementedError

    def calc_effect(self):
        raise NotImplementedError

    def run_pk(self, dt=None):
        if dt is None:
            dt = datetime.now()
        t = (dt - self._dt).total_seconds()
        res = solve_ivp(
            self.pk,
            t_span=[0, t],
            t_eval=[t],
            y0=self._y,
            args=(self._rate, self._params),
        )
        self._dt = dt
        ytmp = np.squeeze(res.y)
        if len(ytmp) == len(self._y):
            self._y = ytmp
        else:
            # TODO: log error
            pass
        return dt, self.calc_cc()

    def run_pkpd(self, dt=None):
        dt, cc = self.run_pk(dt)
        return dt, cc, self.calc_effect()

    def simulate_pk(self, dt):
        t = (dt - self._dt).total_seconds()
        res = solve_ivp(
            self.pk,
            t_span=[0, t],
            y0=self._y,
            args=(self._rate, self._params),
            dense_output=True,
        )
        # ytmp = np.squeeze(res.y)
        # if len(ytmp) != len(self._y):
        #     # TODO: log error
        #     pass
        # return dt, self.calc_cc()
        return None

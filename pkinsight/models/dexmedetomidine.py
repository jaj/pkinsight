from datetime import datetime
from typing import List

from PyTCI.models.dexmedetomidine import Hannivoort

from pkinsight.models.base import Model


class Dexmedetomidine(Model):
    haspd = False

    def __init__(
        self, weight: int, t0: datetime | None = None, y0: List[float] = [0, 0, 0, 0]
    ) -> None:
        self.model = Hannivoort(weight)
        super().__init__(t0, y0)
        self._params = {
            "k12": self.model.k12,
            "k21": self.model.k21,
            "k13": self.model.k13,
            "k31": self.model.k31,
            "k10": self.model.k10,
            # Colin PJ, Hannivoort LN, Eleveld DJ, Reyntjens KMEM, Absalom AR, Vereecke HEM, Struys MMRF.
            # Dexmedetomidine pharmacokinetic-pharmacodynamic modelling in healthy volunteers:
            # 1. Influence of arousal on bispectral index and sedation.
            # Br J Anaesth. 2017 Aug 1;119(2):200-210. doi: 10.1093/bja/aex085. PMID: 28854538.
            "ke0": 0.120 / 60,
            "C50": 2.63,
            "BaseBIS": 96.8,
        }

    def pk(self, t, y, Inf, p):
        A1, A2, A3, E = y
        ddt_A2 = p["k12"] * A1 - p["k21"] * A2
        ddt_A3 = p["k13"] * A1 - p["k31"] * A3
        ddt_A1 = (
            Inf + p["k21"] * A2 + p["k31"] * A3 - A1 * (p["k12"] + p["k13"] + p["k10"])
        )
        ddt_E = p["ke0"] * (A1 - E)
        return ddt_A1, ddt_A2, ddt_A3, ddt_E

    def calc_cc(self):
        A1 = self._y[0]
        return A1

    def calc_effect(self, Cc=None):
        if Cc is None:
            Cc = self.calc_cc()
        E = self._y[3]
        return self._params["BaseBIS"] * (1 - E / (E + self._params["C50"]))

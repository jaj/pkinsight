from pint import UnitRegistry
from pkinsight import assets
from importlib import resources


ureg = UnitRegistry()
Q_ = ureg.Quantity
U_ = ureg.Unit


def load_units():
    asset_source = resources.files(assets)
    with resources.as_file(asset_source) as fp:
        ureg.load_definitions(fp / "units.txt")


load_units()


def calculate_drug_dose(data):
    r = Q_(data["rate"], data["rate_unit"])
    print(f"rate: {r}, {r.dimensionality}")
    v = Q_(data["volume"], data["volume_unit"])
    print(f"volume: {v}, {v.dimensionality}")
    if data["drug_concentration"] is not None:
        c = Q_(data["drug_concentration"])
        print(f"conc: {c}, {c.dimensionality}")
        cr = c * r
        print(f"cr: {cr}, {cr.dimensionality}")
        # print(f"cr: {cr.to('ug/h')}")
        cv = c * v
        print(f"cv: {cv, {cv.dimensionality}}")
        # print(f"cv: {cv.to('ug')}")
    else:
        cr = None
        cv = None
    return (r, cr, v, cv)


def get_config(args):
    conf = {}
    try:
        conf["weight"] = int(args.get("weight")[0].decode("utf-8"))
    except (KeyError, ValueError):
        conf["weight"] = 70

    conf["model"] = args.get("model")[0].decode("utf-8").lower()

    conf["bloc"] = args.get("bloc")[0].decode("utf-8")

    drug = args.get("drug")[0].decode("utf-8")
    if not drug:
        drug = None
    conf["drug"] = drug

    syringeid = args.get("syringeid")[0].decode("utf-8")
    if not syringeid:
        syringeid = None
    conf["syringeid"] = syringeid

    return conf

import pandas as pd
from pint import UnitRegistry

infile = "/home/jaj/Desktop/rtmms_base_terms_2023-09-20.xlsx"
outfile = "./units.txt"

ureg = UnitRegistry()
Q_ = ureg.Quantity
U_ = ureg.Unit


def test_unit(u):
    try:
        unit = U_(u)
        return str(unit)
    except Exception:
        return pd.NA


df = pd.read_excel(
    infile, sheet_name="partition_4", usecols=["RefID", "UOM_UCUM"], dtype="string"
)

df = df.dropna(how="any", axis="rows")

df["UOM_UCUM"] = df["UOM_UCUM"].apply(lambda s: s.split(" "))
df = df.explode("UOM_UCUM")

df["UOM_UCUM"] = df["UOM_UCUM"].apply(test_unit)
df = df.dropna(how="any", axis="rows")

df = df.drop_duplicates(subset="RefID")
print(f"{len(df)} values")

res = df.apply(lambda r: f'{r["RefID"]} = {r["UOM_UCUM"]}\n', axis=1)
with open(outfile, "w") as fd:
    fd.writelines(res.to_list())


# Test
ureg.load_definitions(outfile)
flow = ureg("2 MDC_DIM_MILLI_L_PER_HR")
conc = ureg("50 MDC_DIM_MICRO_G_PER_ML")
print(flow)
print(conc)
print((flow * conc).to("mg/day"))
